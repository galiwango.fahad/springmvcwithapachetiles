package com.apacheDemo.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class HompageController {

@RequestMapping(value= {"/","/index"} , method=RequestMethod.GET)
	public String showHomePage() {
		
		return "index";
	}


@RequestMapping(value="/about_us" , method=RequestMethod.GET)
public String showAboutUsPage() {
	return "about_us";
}

@RequestMapping(value="/image_upload" , method=RequestMethod.GET)
public String toUploadPage() {
		return "image_upload";
}

@RequestMapping(value="/process_upload" , method=RequestMethod.POST)
public String processImageUpload(@RequestParam("file") MultipartFile multipartFile , Model model , HttpServletRequest request) {

if(!multipartFile.isEmpty()) {
	
	try {

		String relattivePath="/resources/image_uploads";//folder to store images
		String absoluteFilePath = request.getRealPath(relattivePath);
		System.out.println("location " +absoluteFilePath);
		byte[] bytes = multipartFile.getBytes();
		File dir =  new File(absoluteFilePath);
		if(!dir.exists()) {//creating the directory if doesnot exists
			dir.mkdirs();
		}
	
		//preparing a file
		File uploadFile = new File(absoluteFilePath+File.separator+multipartFile.getOriginalFilename());
        FileOutputStream fileOutputStream = new FileOutputStream(uploadFile);	
		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
		bufferedOutputStream.write(bytes);
		bufferedOutputStream.flush();
		bufferedOutputStream.close();
		
		FileInputStream fileInputStream = new FileInputStream(uploadFile);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
	
		
		
		model.addAttribute("status","file successfuly uploaded");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	return "image_upload";
}else {
	model.addAttribute("status", "choose atleast one file to upload");
	return "image_upload";
}

	
}
	
}
