<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


    <div id="image_upload">
    <form method="POST" action="/ApacheTilesDemo/process_upload"  enctype="multipart/form-data" >
    <label>image upload</label>
    <input type="file" name="file" >
    <br>
    <input type="submit">
    </form>
    
    <h4>Uploaded image details</h4>
    <div>
    <p><c:out value="${image.getName()}" /></p>
    <p><c:out value="${image.originalFilename}" /> </p>
    <p><c:out value="${image.contentType}"/> </p>
    <p>servlet path: <c:out value="${path}"/> </p>
    <p>servlet real path: <c:out value="${rpath}"/> </p>
    <h4><b>status: </b> <c:out value="${status}"></c:out> </h4>
    </div>
  
    </div>