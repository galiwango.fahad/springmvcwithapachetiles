<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><tiles:getAsString name="title" /></title>
</head>
<body>
	<header id="header">
		<tiles:insertAttribute name="header" />
	</header>


	<div id="main-content">
		<tiles:insertAttribute name="body" />
	</div>



	<div id="footer">
		<tiles:insertAttribute name="footer" />
	</div>

</body>
</html>